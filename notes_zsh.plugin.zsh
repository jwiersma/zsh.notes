#! /bin/zsh

_notes_list() {
  local state

  _arguments \
    '1:command:->commands' \
    '2:note:->notes'

  case $state in
    commands)
      _values 'command arguments' add edit list sync show rm del
    ;;
    notes)
      _files -W ${NOTES_HOME}
    ;;
  esac
}

compdef _notes_list notes || echo "Zsh.notes warning: Compdef not detected, autocompletions won't work.\n"

NOTES_HOME="${HOME}/.notes"

function __test_environment() {
  [[ -d ${NOTES_HOME} ]] || mkdir "${NOTES_HOME}"
  if [[ -n "${NOTES_GIT}" ]]; then
    [[ -d ${NOTES_HOME}/.git ]] || git clone "${NOTES_GIT}" "${NOTES_HOME}"
  fi
  [[ -n "${EDITOR}" ]] || echo "Warning: EDITOR environment variable not set.\n"
}

function __list_notes() {
  tree_output=$(tree "${NOTES_HOME}/${1}" | tail -n +2)
  output_count=$(echo "${tree_output}" | wc -l)

  echo "/${1}"
  echo "${tree_output}" | head -n $(($output_count -1))
}

function __edit_note() {
  ${EDITOR} "${NOTES_HOME}/${1}"
  __sync_notes
}

function __add_note() {
  if [[ -z "${1}" ]]; then
    echo "No filename given.";
    return 1
  fi

  fullpath="${NOTES_HOME}/${1}"
  dir="${fullpath:h}"

  mkdir -p "${dir}"
  ${EDITOR} "${fullpath}"
  __sync_notes
}

function __show_note() {
  cat ${NOTES_HOME}/${1}
}

function __remove_note() {
  rm -r ${NOTES_HOME}/${1}
  __sync_notes
}

function __notes_git_commit() {
  git add .
  git commit -m "$(date)"
  git pull origin master
  git push origin master
}

function __sync_notes() {
  if [[ -n "${NOTES_GIT}" ]]; then
    cwd=$(pwd)
    cd ${NOTES_HOME}
    git status | grep -q 'working tree clean' || __notes_git_commit
    cd ${cwd}
  fi
}

function notes() {
  __test_environment
  case $1 in
    add)
      __add_note $2
      ;;
    edit)
      __edit_note $2
      ;;
    list)
      __list_notes $2
      ;;
    sync)
      __sync_notes
      ;;
    show)
      __show_note $2
      ;;
    rm)
      __remove_note $2
      ;;
    del)
      __remove_note $2
      ;;
    *)
      __list_notes
      ;;
  esac
}

