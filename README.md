# Zsh.notes

Zsh.notes is a simple wrapper that enables you to easily access a notes folder on your local filesystem, and keep it in sync with a git repository.

## Features

* Easily access a local notes folder through the 'notes' command
* Keep your notes in sync with git
* No configuration needed for basic use
* Use any file format you like
* Zsh autocompletion for your notes folder

## Requirements

* git
* zsh

## Installation

### Oh-my-zsh

```
$ git clone https://gitlab.com/jwiersma/zsh.notes.git ~/.oh-my-zsh/custom/plugins/notes_zsh
```

.zshrc:
```
plugins=(
  notes_zsh
)
```

## Configuration

Two variables are used to configure the local folder to store notes, and the git clone url to use to keep the folder in sync.
```
export NOTES_HOME="$HOME/.notes" # Default
export NOTES_GIT="git@gitlab.com:$youruser/notes.git" # Empty by default
```

If git repo is not set the functionality will be ignored. Once set on the next usage of the notes utility git clone will be used inside the notes home directory if no .git folder is detected. Otherwise it will just enable the utility to check for local changes after editting, adding or removing notes.

## Usage

```
notes                 # List all notes
notes add foo/bar.md  # Add a note and open it in $EDITOR, sync with git
notes del foo/bar.md  # Delete a note and sync with git
notes edit foo/bar.md # Edit a note in $EDITOR and sync with git
notes list foo        # List all notes, optionally pass a folder
notes rm foo/bar.md   # Delete a note and sync with git
notes show foo/bar.md # Cat the content of a note
notes sync            # Sync the notes folder with $NOTES_GIT
```
